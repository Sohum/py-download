import youtube_dl
import sys

if len(sys.argv)<2 or sys.argv[1] not in ['audio', 'video']:
	print("correct usage: python download_music.py (audio|video) urls...")
	sys.exit(1)
format = sys.argv.pop(1)
formats=dict(audio='bestaudio/best',video='mp4')

# help(youtube_dl	)
postprocessors = []
if format=='audio':
	postprocessors += [{
		'key': 'FFmpegExtractAudio',
		'preferredcodec': 'mp3',
		'preferredquality':'192'
	}]
postprocessors += [
    {'key': 'EmbedThumbnail'},
    {'key': 'FFmpegMetadata'}] 

ydl_opts={
	'format': formats.get(format),
	'audioformat':'mp3',
    'noplaylist' : True, 
    # 'youtube_include_dash_manifest': False,
    'no_check_certificate': True,
	'postprocessors': postprocessors,
}
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    for url in sys.argv[1:]:
        try:
            ydl.download([url]) #[url.replace("https://", "http://")])
        except Exception as e:
            print(e) 
'''
https://www.youtube.com/watch?v=9MJAg0VDgO0
https://www.youtube.com/watch?v=FM7MFYoylVs
https://www.youtube.com/watch?v=aatr_2MstrI
https://www.youtube.com/watch?v=9MJAg0VDgO0
https://www.youtube.com/watch?v=9MJAg0VDgO0
'''
