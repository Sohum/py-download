/* kmp search namespace
 */
var kmp = {
	build_auxiliary_array : function(s) {
		if(s == undefined || s.length == 0)
			return undefined;
		var n = s.length;
		var dp = new Array(n);
		dp[0]=0;
		for(var i=1; i<n; i++) {
			var j = i-1;
			while(j>-1 && s[dp[j]]!=s[i]) {
				j=dp[j]-1;
			}
			dp[i] = (j>-1)? dp[j]+1 : (s[i]==s[0])?1:0;
		}
		return dp;
	},

	search : function(s, target, dp) {
		var m = 0, n = 0;
		while(m<s.length) {
			if(s[m]==target[n]){
				// console.log("here1",m,n);
				m++; n++;
				if(n==target.length) return true;
			} else if(n>0) {
				// console.log("here2",m,n);
				n=dp[n-1];
			} else {
				// console.log("here3",m,n);
				m++;
			}
		}
		return false;
	},

	merge_all : function(arrays) {
		all = [];
		arrays.forEach(function(e) { if(e.length>0) all.push(e)});
		if(all.length==0) return all;
		
		// all indices initialized to 0
		var done = 0, n = all.length;
		var counter = [], ans = [];
		for(var i = 0; i<n; i++) counter.push(0);
		// start merging
		while(done<n) {
			var top_arrays = [];
			var i;
			for(var i = 0; i<n-1 && counter[i]==all[i].length; i++); 
			current_min = all[i][counter[i]];

			for(var i = 0; i<n; i++) {			
				if(counter[i] == all[i].length)	continue;
				if(current_min>all[i][counter[i]]) {
					top_arrays = [i];
					current_min = all[i][counter[i]];
				} else if(current_min == all[i][counter[i]]) {
					top_arrays.push(i);
				}
			}

			ans.push(current_min);
			top_arrays.forEach(function(ele) {
				counter[ele]++;
			});
			done = 0;
			for(var i=0; i<n; i++) {
				if(counter[i] == all[i].length) done++;
			}
		}
		return ans;
	},

	get_searched_songs : function(complete_query, songs) {
		query = {}
		complete_query.toLowerCase().split(' ').forEach(function(ele) {
			query[ele] = 1;
		});
		var ans = [];
		for(var t in query) {
			ans.push([]);
			var dp = kmp.build_auxiliary_array(t);
			for (var i = 0;i < songs.length; i++){
				if(kmp.search(songs[i].toLowerCase(), t, dp)) {
					ans[ans.length-1].push(i);
				}
			}			
		}
		return kmp.merge_all(ans);
	}

}