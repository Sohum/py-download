import SimpleHTTPServer
import SocketServer

handler = SimpleHTTPServer.SimpleHTTPRequestHandler
httpd = SocketServer.TCPServer(("",8080),handler)

httpd.serve_forever()
